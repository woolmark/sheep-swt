WoolMark for SWT
============

Development Environment
------------

1. Import this project into eclipse workspace
2. Download the SWT project for your OS from https://www.eclipse.org/swt/
3. Import SWT project into eclipse workspace
4. Add SWT project as a dependency for this project

for more details, https://www.eclipse.org/swt/eclipse.php

License
------------
WTFPL

