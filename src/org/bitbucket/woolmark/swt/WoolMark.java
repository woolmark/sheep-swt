package org.bitbucket.woolmark.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class WoolMark {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		shell.setText("WoolMark");
		new WoolMarkCanvas(shell, display);

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
		System.exit(0);
	}
}
